require 'spec_helper'

emptySms = {
  desc: 'returns 0 and not unicode if sms is empty',
  string: '',
  is_unicode: false,
  char_count: 0
}
# tests factorizations functions
def expected_result(obj, exlcude_keys = [])
  result = obj.clone
  ([:string, :desc, :max_sms] + exlcude_keys).each do |key|
    result.delete(key)
  end
  result
end

def test_sms_count_obj(obj)
  expect(DueSmsCounter.calc_char_and_sms_count(obj[:string])).to eq(expected_result(obj))
end

def test_count_obj(obj)
  expect(DueSmsCounter.calc_char_count(obj[:string])).to eq(expected_result(obj, [:sms_count]))
end

def test_max_char_obj(obj)
  expect(DueSmsCounter.calc_max_char_count(obj[:max_sms])).to eq(expected_result(obj));
end

# tested contents
# sms char counts tests
emptySms = {
  desc: 'returns 0 and not unicode if sms is empty',
  string: '',
  is_unicode: false,
  char_count: 0,
  sms_count: 0,
};

gsm7With2SizedChars = {
  desc: 'returns more than char.count and not unicode if sms is has only gsm7 chars and some 2sized chars',
  string: '|o|',
  is_unicode: false,
  char_count: 5,
  sms_count: 1
}

gsm7Without2SizedChars = {
  desc: 'return char.count and not unicode',
  string: 'this is my tested string, even if its a very very very very very very very very very very very very very very very very very very very very very very long one, this should works perfectly (size = 200)',
  is_unicode: false,
  char_count: 200,
  sms_count: 2
}

unicodeSms = {
  desc: 'return is unicode with right char.count',
  string: 'unicºde string with some gsm72sized chars => |',
  is_unicode: true,
  char_count: 46,
  sms_count: 1
}

twoSizedGsm7Sms = {
  desc: 'gsm7 sms in 2 sms',
  string: 'this is an gsm7 |o| sms with more char than for one sms but which can be send with 2 sms. So its long long long long long long long long long long long long long',
  char_count: 163,
  is_unicode: false,
  sms_count: 2
}

fourSizedGsm7Sms = {
  desc: 'unicode sms in 4 sms',
  string: 'this is ˙´®oiu¯ßð an gsm7 |o| sms with more char than for one sms but which can be send with 2 sms. So its long long long long long long long long long long long long long',
  char_count: 171,
  is_unicode: true,
  sms_count: 3
}

# max sms char tests
emptyCount = {
  desc: 'max_sms = 0 should return empty 0 for both',
  max_sms: 0,
  gsm7: 0,
  unicode: 0
}

oneSmsCount = {
  desc: 'max_sms = 1 should return empty 160(gsm7) and 70(unicode)',
  max_sms: 1,
  gsm7: 160,
  unicode: 70
}

tenCount = {
  desc: 'max_sms = 10 should return empty 1520(gsm7) and 660(unicode)',
  max_sms: 10,
  gsm7: 1520,
  unicode: 660
}

overSizedCount = {
  desc: 'max_sms = 100 should return max for each',
  max_sms: 100,
  gsm7: 1600,
  unicode: 1600
}


RSpec.describe DueSmsCounter do
  it 'has a version number' do
    expect(DueSmsCounter::VERSION).not_to be nil
  end

  [emptySms, gsm7With2SizedChars, gsm7Without2SizedChars, unicodeSms].each do |obj|
    it obj[:desc] do
      test_count_obj(obj)
    end
  end

  [emptyCount, oneSmsCount, tenCount, overSizedCount].each do |obj|
    it obj[:desc] do
      test_max_char_obj(obj)
    end
  end

  it('sould be sendable sms') do
    expect(DueSmsCounter.can_be_sent?(unicodeSms[:string], 1)).to be(true)
  end

  it('2 sized gsm7 sms sould be send if size >= 2') do
    expect(DueSmsCounter.can_be_sent?(twoSizedGsm7Sms[:string], 1)).to be(false)
    expect(DueSmsCounter.can_be_sent?(twoSizedGsm7Sms[:string], 2)).to be(true)
    expect(DueSmsCounter.can_be_sent?(twoSizedGsm7Sms[:string], 4)).to be(true)
  end

  it('4 sized unicode sms sould be send if size >= 4') do
    expect(DueSmsCounter.can_be_sent?(fourSizedGsm7Sms[:string], 1)).to be(false)
    expect(DueSmsCounter.can_be_sent?(fourSizedGsm7Sms[:string], 2)).to be(false)
    expect(DueSmsCounter.can_be_sent?(fourSizedGsm7Sms[:string], 4)).to be(true)
    expect(DueSmsCounter.can_be_sent?(fourSizedGsm7Sms[:string], 5)).to be(true)
  end

  [emptySms, gsm7With2SizedChars, gsm7Without2SizedChars, unicodeSms, twoSizedGsm7Sms, fourSizedGsm7Sms].each do |obj|
    it obj[:desc] do
      test_sms_count_obj(obj)
    end
  end
end
