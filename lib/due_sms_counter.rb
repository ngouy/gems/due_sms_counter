require 'due_sms_counter/version'
require 'json'

CONSTS = JSON.parse(File.read(File.join(File.dirname(__FILE__), 'due_sms_counter/consts.json')))

B7_2SIZED_CHARS, B7_BASE_CHARS, B7_SMS_COUNT, UNICODE_SMS_COUNT = CONSTS.values_at(*%w[B7_2SIZED_CHARS B7_BASE_CHARS B7_SMS_COUNT UNICODE_SMS_COUNT])
B7_CHARS = B7_BASE_CHARS + B7_2SIZED_CHARS

module DueSmsCounter
  class << self

    def can_be_sent?(sms, max_sms)
      char_count = calc_char_count(sms)
      max_char   = calc_max_char_count(max_sms)
      is_unicode = char_count[:is_unicode]

      char_count[:char_count] <= max_char[is_unicode ? :unicode : :gsm7]
    end

    def calc_char_count(sms)
      sms = (sms || '').split('')
      is_7_bits = (sms + B7_CHARS).uniq.length == B7_CHARS.length
      if is_7_bits
        { char_count: sms.length + count_2chars_sized(sms),
          is_unicode: false }
      else
        { char_count: sms.length,
          is_unicode: true }
      end
    end

    def calc_max_char_count(sms_count)
      {
        gsm7: (
          B7_SMS_COUNT[sms_count.to_s] ||
          B7_SMS_COUNT['max']
        )[1],
        unicode: (
          UNICODE_SMS_COUNT[sms_count.to_s] ||
          UNICODE_SMS_COUNT['max']
        )[1]
      }
    end

    def calc_sms_count(char_count, is_unicode)
      base = is_unicode ? UNICODE_SMS_COUNT : B7_SMS_COUNT
      sms_count = 0
      while 42 do
        break if !base[sms_count.to_s] || (base[sms_count.to_s][1] >= char_count)
        sms_count += 1
      end
      sms_count
    end

    def calc_char_and_sms_count(sms)
      result = calc_char_count(sms)
      result[:sms_count] = calc_sms_count(result[:char_count], result[:is_unicode])
      result
    end

    def count_2chars_sized(sms)
      sms.reduce(0) do |count, letter|
        count + (B7_2SIZED_CHARS.include?(letter) ? 1 : 0)
      end
    end
  end
end
